package com.example.moviebrowser;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.moviebrowser.model.Movie;

import org.w3c.dom.Text;

import java.util.List;

public class MovieAdapter extends ArrayAdapter<Movie> {
    private final Context context;
    private final List<Movie> movies;

    MovieAdapter(Context context, List<Movie> movies) {
        super(context, R.layout.rowtemplate, movies);

        this.context = context;
        this.movies = movies;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View rowView = inflater.inflate(R.layout.rowtemplate, null, true);
        ((TextView)rowView.findViewById(R.id.title)).setText(movies.get(position).getTitle());
        ((TextView)rowView.findViewById(R.id.genre)).setText(movies.get(position).getGenre().getGenre());
        ((TextView)rowView.findViewById(R.id.rating)).setText(movies.get(position).getImdb_rate());

        return rowView;
    }
}
