package com.example.moviebrowser.model;

import android.app.Activity;
import android.support.annotation.NonNull;

import java.util.List;

public class Movie {
    private int id;
    private String title;
    private Studio studio;
    private Director director;
    private Genre genre;
    private String release;
    private String description;
    private String budget;
    private String imdb_rate;
    private List<Actor> actors;

    public List<Actor> getActors() {
        return actors;
    }
    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Studio getStudio() {
        return studio;
    }

    public void setStudio(Studio studio) {
        this.studio = studio;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getImdb_rate() {
        return imdb_rate;
    }

    public void setImdb_rate(String imdb_rate) {
        this.imdb_rate = imdb_rate;
    }

    public String getActorNames() {
        String ret = actors.get(0).toString();

        for (int i = 1; i < actors.size(); i++) {
            ret += System.getProperty("line.separator");
            ret += actors.get(i).toString();
        }

        return ret;
    }

    @NonNull
    @Override
    public String toString() {
        return this.title;
    }
}
