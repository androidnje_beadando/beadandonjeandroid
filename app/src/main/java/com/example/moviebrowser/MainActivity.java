package com.example.moviebrowser;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.example.moviebrowser.model.Movie;
import com.example.moviebrowser.model.Studio;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static DatabaseHelper sqliteDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set toolbar
        Toolbar myToolbar = (Toolbar)findViewById(R.id.main_toolbar);
        setSupportActionBar(myToolbar);

        // Load browse fragment
        loadFragment(new BrowseFragment(), "browse", false);
        sqliteDB = new DatabaseHelper(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_about:
                loadFragmentWithBackStack(new AboutFragment(), "about");
                return true;

            case R.id.action_add:
                loadFragmentWithBackStack(new NewItemFragment(), "newItem");
                return true;
        }

        return true;
    }

    public void loadFragmentWithBackStack(Fragment fragment, String tag) {
        loadFragment(fragment, tag, true);
    }

    public void loadFragment(Fragment fragment, String tag, boolean toStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment, tag);

        if (toStack)
            fragmentTransaction.addToBackStack(tag);

        fragmentTransaction.commit();
    }
}
